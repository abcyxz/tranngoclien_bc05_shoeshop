import React, { Component } from 'react'

export default class ItemShoe extends Component {
  render() {
    let {image,name} = this.props.data;
    return (
      <div className='col-3 p-1'>
        <div class="card text-left h-100">
          <img class="card-img-top" src={image} alt="" />
          <div class="card-body">
            <h4 class="card-title">{name}</h4>
            <button
            onClick={() => {
                this.props.handleAddToCart(this.props.data);
            }}
            className="btn btn-success mr-5">Buy</button>
            <button
            onClick={()=> {
                this.props.handleViewDetail(this.props.data);
            }}
            className="btn btn-primary"
            >Detail</button>
          </div>
        </div>
        
      </div>
    )
  }
}
