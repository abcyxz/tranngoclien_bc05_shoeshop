import React, { Component } from 'react';
import Cart from "./Cart";
import {dataShoe} from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ItemShoe from "./ItemShoe";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop extends Component {
state = {
  shoeArr: dataShoe,
  detail: dataShoe[1],
  cart: [],
};

handleChangeDetail = (value) => {
  this.setState( {detail: value});
};
handleAddToCart = (shoe) => {
  let cloneCart = [...this.state.cart];
  let index = this.state.cart.findIndex((item) => {
    return item.id == shoe.id;
  });
  if (index == -1) {
    let cartItem = {...shoe, number:1};
    cloneCart.push(cartItem);
  }else {
    cloneCart[index].number++;
  }
  this.setState({
    cart: cloneCart,
  })
}
handleUpCart = (id) => {
  let cloneCart = [...this.state.cart];
  console.log(cloneCart);
  let index = this.state.cart.findIndex((item) => {
    return item.id == id;
  })
  if (index !== -1) {
    cloneCart[index].number++;
  }
  this.setState({
    cart: cloneCart // 
  })
}

handleDownCart = (id) => {
  let cloneCart = [...this.state.cart];
  console.log(cloneCart);
  let index = this.state.cart.findIndex((item) => {
    return item.id == id;
  })
  if (index !== -1) {
    cloneCart[index].number--;
  }
  this.setState({
    cart: cloneCart // 
  })
}

  render() {
    return (
      <div className='container'>
        <Cart cart={this.state.cart} handleUpCart={this.handleUpCart} handleDownCart={this.handleDownCart}/>
        <ListShoe 
        handleAddToCart = {this.handleAddToCart}
        handleChangeDetail = {this.handleChangeDetail}
        shoeArr={this.state.shoeArr} />
        <DetailShoe detail={this.state.detail} />
      </div>
    )
  }
}
