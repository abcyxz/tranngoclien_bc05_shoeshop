import React, { Component } from 'react'

export default class Cart extends Component {
    renderTbody = () => {
        return this.props.cart.map((item) => {
            return (
                <tr>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.price * item.number}</td>
                    <td>
                        <button  onClick={() => {
                            this.props.handleDownCart(item.id);
                        }}>-</button>
                        {item.number}
                        <button
                         onClick={() => {
                            this.props.handleUpCart(item.id);
                        }}
                        >+</button>
                    </td>
                    <td>
                        <img style={{width: "80px"}} src={item.image} alt="" />
                    </td>
                </tr>
            )
        })
    }
  render() {
    return (
        <table className='table'>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Image</th>
                </tr>
            </thead>
            <tbody>{this.renderTbody()}</tbody>
        </table>
     
    )
  }
}
